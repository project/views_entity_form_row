<?php

declare(strict_types=1);

namespace Drupal\views_entity_form_row;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service provider class.
 */
final class FormOperations implements ContainerInjectionInterface {

  /**
   * The ViewsEntityFormRowInfo service.
   *
   * @var \Drupal\views_entity_form_row\ViewsEntityFormRowInfo
   */
  private ViewsEntityFormRowInfo $viewsEntityFormRowInfo;

  /**
   * The RouteMatchInterface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private RouteMatchInterface $currentRouteMatch;

  /**
   * Constructs a new FormOperations object.
   *
   * @param \Drupal\views_entity_form_row\ViewsEntityFormRowInfo $views_entity_form_row_info
   *   The ViewsEntityFormRowInfo service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The route match.
   */
  public function __construct(ViewsEntityFormRowInfo $views_entity_form_row_info, RouteMatchInterface $current_route_match) {
    $this->viewsEntityFormRowInfo = $views_entity_form_row_info;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('views_entity_form_row.info'),
      $container->get('current_route_match')
    );
  }

  /**
   * Custom function to alter node form.
   */
  public function nodeFormAlter(array &$form, FormStateInterface $form_state, string $form_id): void {
    if (!$this->viewsEntityFormRowInfo->isPluginUsedInCurrentView()) {
      return;
    }
    $form['actions']['submit']['#submit'][] = [
      $this,
      'redirectToCurrentPageSubmit',
    ];
  }

  /**
   * Custom function to redirect to current route.
   */
  public function redirectToCurrentPageSubmit(array &$form, FormStateInterface $form_state): void {
    $url = Url::fromRouteMatch($this->currentRouteMatch);
    $form_state->setRedirectUrl($url);
  }

}
