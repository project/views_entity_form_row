<?php

declare(strict_types=1);

namespace Drupal\views_entity_form_row;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\node\NodeForm;
use Drupal\views_entity_form_row\Form\ViewsEntityFormRowNodeForm;

/**
 * Service provider class.
 */
final class EntityOperations {

  /**
   * Custom function to alter entity type.
   */
  public function entityTypeAlter(array &$entity_types): void {
    $node = $entity_types['node'];
    \assert($node instanceof EntityTypeInterface);
    foreach ($node->getHandlerClasses()['form'] as $operation => $class) {
      if (!\is_a($class, NodeForm::class, TRUE)) {
        continue;
      }
      $node->setFormClass($operation, ViewsEntityFormRowNodeForm::class);
    }
  }

}
