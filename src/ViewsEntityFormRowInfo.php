<?php

namespace Drupal\views_entity_form_row;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\ViewEntityInterface;
use Drupal\views_entity_form_row\Plugin\views\row\EntityFormRow;

/**
 * Service provider class.
 */
class ViewsEntityFormRowInfo {
  /**
   * The EntityStorageInterface.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private EntityStorageInterface $viewStorage;

  /**
   * The RouteMatchInterface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private RouteMatchInterface $currentRouteMatch;

  /**
   * Constructs a new ViewsEntityFormRowInfo object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The route match.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $current_route_match) {
    $this->currentRouteMatch = $current_route_match;
    $this->viewStorage = $entity_type_manager->getStorage('view');
  }

  /**
   * Custom function to check row plugin usage in view.
   */
  public function isPluginUsedInCurrentView(): bool {
    $view_id = $this->currentRouteMatch->getParameter('view_id');
    $display_id = $this->currentRouteMatch->getParameter('display_id');
    if ($view_id === NULL || $display_id === NULL) {
      return FALSE;
    }
    $view = $this->viewStorage->load($view_id);
    \assert($view instanceof ViewEntityInterface);
    $view_executable = $view->getExecutable();
    $view_executable->build($display_id);

    return $view_executable->rowPlugin instanceof EntityFormRow;
  }

}
