<?php

declare(strict_types=1);

namespace Drupal\views_entity_form_row\Form;

/**
 * The trait ViewsEntityFormRowTrait implementation.
 */
trait ViewsEntityFormRowTrait {

  /**
   * {@inheritdoc}
   */
  public function doGetFormId(): string {
    $form_id = $this->entity->getEntityTypeId();
    if ($this->entity->getEntityType()->hasKey('bundle')) {
      $form_id .= '_' . $this->entity->bundle();
    }
    if ($this->operation != 'default') {
      $form_id .= '_' . $this->operation;
    }

    // Make the form id unique to allow multiple instances of the form works.
    if ($this->viewsEntityFormRowInfo->isPluginUsedInCurrentView() && $this->entity->id()) {
      $form_id .= '_' . $this->entity->id();
    }

    return $form_id . '_form';
  }

}
