<?php

declare(strict_types=1);

namespace Drupal\views_entity_form_row\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\node\NodeForm;
use Drupal\views_entity_form_row\ViewsEntityFormRowInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation of ViewsEntityFormRowNodeForm class.
 */
class ViewsEntityFormRowNodeForm extends NodeForm {

  use ViewsEntityFormRowTrait;

  /**
   * The ViewsEntityFormRowInfo service.
   *
   * @var \Drupal\views_entity_form_row\ViewsEntityFormRowInfo
   */
  private ViewsEntityFormRowInfo $viewsEntityFormRowInfo;

  /**
   * Constructs a new ViewsEntityFormRowNodeForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The private tempstore factory service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\views_entity_form_row\ViewsEntityFormRowInfo $views_entity_form_row_info
   *   The ViewsEntityFormRowInfo service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The bundle information.
   * @param \Drupal\Component\Datetime\TimeInterface|null $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, PrivateTempStoreFactory $temp_store_factory, AccountInterface $current_user, DateFormatterInterface $date_formatter, ViewsEntityFormRowInfo $views_entity_form_row_info, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $temp_store_factory, $entity_type_bundle_info, $time, $current_user, $date_formatter);
    $this->viewsEntityFormRowInfo = $views_entity_form_row_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('tempstore.private'),
      $container->get('current_user'),
      $container->get('date.formatter'),
      $container->get('views_entity_form_row.info'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return $this->doGetFormId();
  }

}
