<?php

declare(strict_types=1);

namespace Drupal\views_entity_form_row\Plugin\Derivative;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\ViewsData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides derivative plugins for the EntityFormRow plugin.
 *
 * @ingroup views_row_plugins
 *
 * @see \Drupal\views\Plugin\views\row\EntityRow
 */
final class EntityFormRow implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The array.
   *
   * @var derivatives
   */
  protected array $derivatives = [];

  /**
   * The string.
   *
   * @var basePluginId
   */
  protected string $basePluginId;

  /**
   * The EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The ViewsData.
   *
   * @var Drupal\views\ViewsData
   */
  protected ViewsData $viewsData;

  /**
   * Constructs a new EntityFormRow object.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\views\ViewsData $views_data
   *   The views data.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager, ViewsData $views_data) {
    $this->basePluginId = $base_plugin_id;
    $this->entityTypeManager = $entity_type_manager;
    $this->viewsData = $views_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id): self {
    return new self(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('views.views_data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinition($derivative_id, $base_plugin_definition) {
    if (!empty($this->derivatives) && !empty($this->derivatives[$derivative_id])) {
      return $this->derivatives[$derivative_id];
    }
    $this->getDerivativeDefinitions($base_plugin_definition);
    return $this->derivatives[$derivative_id];
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      // Just add support for entity types which have a views integration.
      if (($base_table = $entity_type->getBaseTable()) && $this->viewsData->get($base_table)) {
        $this->derivatives[$entity_type_id] = [
          'id' => 'views_entity_form_row:' . $entity_type_id,
          'provider' => 'views_entity_form_row',
          'title' => $entity_type->getLabel() . ' - Entity Form',
          'help' => $this->t('Display the @label', ['@label' => $entity_type->getLabel()]),
          'theme' => 'views_entity_form_row',
          'base' => [$entity_type->getDataTable() ?: $entity_type->getBaseTable()],
          'entity_type' => $entity_type_id,
          'display_types' => ['normal'],
          'class' => $base_plugin_definition['class'],
        ];
      }
    }

    return $this->derivatives;
  }

}
